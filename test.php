<?php

namespace frontend\modules\product\controllers;

use common\behaviors\QueryParamAuth;
use common\components\ProductCreator;
use common\helpers\FileHelper;
use common\models\product\Calculation;
use common\models\product\CalculationComment;
use common\models\product\ReleasedProduct;
use common\models\product\TypedProduct;
use common\models\product\Category;
use common\models\product\ProductCompanyDelegate;
use common\models\product\ReleasedDocument;
use common\models\company\Company;
use yii;
use common\base\FrontController;
use common\models\product\Product;

/**
 * Контроллер для работы с продуктами
 * Class ProductController
 * @package frontend\modules\product\controllers
 */
class ProductController extends FrontController
{
    protected $servicePermitions = ['get-rendered-document'];

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                'optional' => $this->servicePermitions
            ]
        ]);
    }

    /** 
     * Получение списка категорий продукта 
     **/
    public function actionCategoryList() 
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => []
        ];
        
        $result['list'] = Category::find()->indexBy('id')->orderBy('sort')->all();
        $result['success'] = true;
        
        return $result;
    }

    /**
     * Получение продукта по id
     **/
    public function actionGetById($id)
    {
        $result = [
            'success' => true,
            'messages' => [],
            'errors' => [],
            'product' => []
        ];

        $result['product'] = Product::getById($id);
        $result['success'] = true;

        return $result;
    }
    
    /**
     * Получение доступных продуктов компании
     **/
    public function actionGetCompanyProducts() 
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'list' => []
        ];
        
        if (!empty(Yii::$app->request->post('delegation_data_by_company_id'))) {
            $result['list'] = Product::getCompanyProducts($this->user['company_id'], false, false, Yii::$app->request->post('delegation_data_by_company_id'));
        } else {
            $result['list'] = Product::getCompanyProducts($this->user['company_id'], false);
        }

        
        $result['success'] = true;
        
        return $result;
    }

    /**
     * Делегирование продукта от СК партнеру (или подразделению)
     * @return array
     */
    public function actionDelegate()
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'list' => []
        ];
        
        $delegationId = Yii::$app->request->post('product_company_delegate_id');
        
        if (empty($delegationId)) {
            $delegation = new ProductCompanyDelegate();
        } else {
            $delegation = ProductCompanyDelegate::getById($delegationId);
            
            if (empty(Yii::$app->request->post('active')) && $delegation) {
                if (ProductCompanyDelegate::removeDelegation($delegationId, true, $this->user['id'])) {
                    $result['success'] = true;
                    $result['messages']['success'][] = 'Настройки продукта сохранены.';
                    return $result;
                }
            }
        }

        $delegationId = $delegation->saveDelegation(Yii::$app->request->post(), $this->user['company_id'], $this->user['id']);
        
        if ($delegationId) { //Сохраняем запись
            $result['product_company_delegate_id'] = $delegationId;
            $result['success'] = true;
        } else {
            $result['errors'] = $delegation->getErrors();
        }
        
        if ($result['success']) {
            $result['messages']['success'][] = 'Настройки продукта сохранены.';
        }
        
        return $result;
    }
    
    /** 
     * Создание формы продукта.
     * Показывается только первый (общий для продуктов) шаг формы
     **/
    public function actionCreateForm($id)
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'product' => [],
            'typedProduct' => [],
            'calculation' => [
                'id' => 0
            ],
            'form' => []
        ];
        
        $product = Product::getById($id, true);
        if (empty($product)) {
            $result['errors'][] = 'Продукт не найден';
            return $result;
        }
        
        $product['name'] = Product::getNameById($id);

        $result['typedProduct'] = TypedProduct::getById($product['typed_product_id']);

        $formClass = Product::getFormClassName($id);
        
        if (empty($formClass['errors'])) {
            /* @var EditorForm $form */
            $form = new $formClass();
            $form->setTemplatePart(1);
            $form->insuranceId = $product['insurance_company_id'];
            $form->companyId = $this->user['company_id'];
            $result['form'] = $form->getResult();
            $result['success'] = true;
        } else {
            $result['errors'] = $formClass['errors'];
        }

        $result['product'] = $product;

        $calculation = new Calculation();
        $calculation->user_id = $this->user['id'];
        $calculation->user_company_id = $this->user['company_id'];
        $calculation->insurance_company_id = $product['insurance_company_id'];
        $calculation->product_id = $id;
        $calculation->status_id = Calculation::STATUS_LIST[1]['id'];
        $calculation->product_version = $product['version'];

        if ($calculation->save()) {
            $result['calculation']['id'] = $calculation->id;
        }
        
        return $result;
    }

    /**
     * Создание формы продукта.
     * Показывается только первый (общий для продуктов) шаг формы
     **/
    public function actionCreateFilledFormByCalculation($id)
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'product' => [],
            'typedProduct' => [],
            'calculation' => [
                'id' => $id,
                'form_data' => [],
                'step' => 1,
                'status_id' => 0,
                'sum' => 0
            ],
            'form' => []
        ];

        $calculation = Calculation::find()
            ->where([
                'is_deleted' => false,
                'id' => $id
            ])
            //id//->andWhere(['<', 'status_id', Calculation::STATUS_LIST[7]['id']]) //id// Просмотр расчета запрещен, если полис выпущен. Временно отключил, сейчас неудобно тестить возможно найду лучшее решение
            ->asArray()
            ->one();

        if (empty($calculation)) {
            $result['errors'][] = 'Расчет не найден';
            return $result;
        }

        $result['calculation']['form_data'] = json_decode($calculation['form_data'], true);
        $result['calculation']['step'] = $calculation['step'];
        $result['calculation']['status_id'] = $calculation['status_id'];
        $result['calculation']['sum'] = $calculation['sum'];

        $product = Product::getById($calculation['product_id'], true);
        if (empty($product)) {
            $result['errors'][] = 'Продукт не найден';
            return $result;
        }

        $product['name'] = Product::getNameById($id);

        $result['typedProduct'] = TypedProduct::getById($product['typed_product_id']);

        $formClass = Product::getFormClassName($calculation['product_id']);

        if (empty($formClass['errors'])) {
            /* @var EditorForm $form */
            $form = new $formClass();
            $form->setTemplatePart(0);
            $form->insuranceId = $product['insurance_company_id'];
            $form->companyId = $this->user['company_id'];
            $result['form'] = $form->getResult();
            $result['success'] = true;
        } else {
            $result['errors'] = $formClass['errors'];
        }

        $result['product'] = $product;

        return $result;
    }

    /**
     * Создание полной формы продукта.
     **/
    public function actionGetEmptyFullForm($id)
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'form' => []
        ];

        $product = Product::getById($id, true);
        if (empty($product)) {
            $result['errors'][] = 'Продукт не найден';
            return $result;
        }

        $product['name'] = Product::getNameById($id);

        $result['typedProduct'] = TypedProduct::getById($product['typed_product_id']);

        $formClass = Product::getFormClassName($id);

        if (empty($formClass['errors'])) {
            /* @var EditorForm $form */
            $form = new $formClass();
            $form->setTemplatePart(0);
            $form->insuranceId = $product['insurance_company_id'];
            $form->companyId = $this->user['company_id'];
            $result['form'] = $form->getResult();
            $result['success'] = true;
        } else {
            $result['errors'] = $formClass['errors'];
        }

        return $result;
    }

    /**
     * Получение полей шага формы
     **/
    public function actionGetFormPart($id)
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'product' => [],
            'form' => []
        ];

        $formClass = Product::getFormClassName($id);
        /* @var EditorForm $form */
        $form = new $formClass();
        $form->setTemplatePart(Yii::$app->request->post('step'));

        $result['form'] = $form->getResult();

        $result['success'] = true;

        return $result;
    }

    /**
     * Получение отрендеренного файла документа
     **/
    public function actionGetRenderedDocument()
    {
        $result = [
            'success' => false,
            'messages' => [],
            'errors' => [],
            'file' => '',
            'sernum' => ''
        ];

        //Получение превью
        if (Yii::$app->request->post('preview')) {
            $previewFile = FileHelper::getUserTmpFolder($this->user['id']) . Yii::$app->request->post('filename');

            if (is_file($previewFile)) {

                $result['success'] = true;

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=preview.pdf');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');

                $result['file'] = base64_encode(file_get_contents($previewFile));

                return $result;
                

                @unlink($previewFile);
            } else {
                echo 'Ошибка. Отсутствуют необходимые данные для завершения операции.';
            }

            //die;
        } else if(Yii::$app->request->get('preview')) {
            $previewFile = FileHelper::getUserTmpFolder($this->user['id']) . Yii::$app->request->get('filename');

            if (is_file($previewFile)) {

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=preview.pdf');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                
                echo file_get_contents($previewFile);

                @unlink($previewFile);
            } else {
                echo 'Ошибка. Отсутствуют необходимые данные для завершения операции.';
            }
        }

        //Получение файла документа
        //if (Yii::$app->request->post('document') && Yii::$app->request->post('id')) {

        //}

        //Скачивание отрендеренного файла документа
        if (Yii::$app->request->post('download')) {

            $downloadFile = Yii::$app->request->post('filename');
            $documentId = Yii::$app->request->post('id');
            //Берем документ по id
            $releasedDocument = ReleasedDocument::find()->where(['id' => $documentId])->one();
            if ($releasedDocument) {
                //Страховая компания
                $insuranceCompany = Company::getById($releasedDocument['insurance_company_id'], true);

                $m_Y = explode('-', $releasedDocument['created_date']);

                $salt = 'F^*572d&83e6of^*O%&Dddr3s*P^RFpfdiC(*0-[98743e46drf@678FCVT&d5768R5467i8d6';

                $path = Yii::getAlias('@app') . '/files/released-documents/' . $insuranceCompany['code'] . '/' . $m_Y[1].'_'.$m_Y[0] . '/' . $documentId . '.pdf';

                if ($downloadFile == md5(md5($path).$salt)) {
                    if (is_file($path)) {

                        $result['success'] = true;
                        $result['sernum'] = $releasedDocument['seria'] . '-' . $releasedDocument['number'];

                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename=' . $releasedDocument['seria'] . '-' . $releasedDocument['number'] . '.pdf');
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');

                        //echo file_get_contents($path);
                        //$result['filename'] = file_get_contents($path);

                        $result['file'] = base64_encode(file_get_contents($path));

                        return $result;

                        //@unlink($downloadFile);
                    } else {
                        echo 'Ошибка. Файл не найден!';
                    }
                } else {
                    echo 'Не указан путь к файлу!';
                }
            } else {
                echo 'Ошибка. Отсутствуют необходимые данные для завершения операции.';
            }

            die;

        } else if(Yii::$app->request->get('download')) {

            $downloadFile = Yii::$app->request->get('filename');
            $documentId = Yii::$app->request->get('id');
            //Берем документ по id
            $releasedDocument = ReleasedDocument::find()->where(['id' => $documentId])->one();
            if ($releasedDocument) {
                //Страховая компания
                $insuranceCompany = Company::getById($releasedDocument['insurance_company_id'], true);

                $m_Y = explode('-', $releasedDocument['created_date']);

                $salt = 'F^*572d&83e6of^*O%&Dddr3s*P^RFpfdiC(*0-[98743e46drf@678FCVT&d5768R5467i8d6';

                $path = Yii::getAlias('@app') . '/files/released-documents/' . $insuranceCompany['code'] . '/' . $m_Y[1].'_'.$m_Y[0] . '/' . $documentId . '.pdf';

                if ($downloadFile == md5(md5($path).$salt)) {
                    if (is_file($path)) {

                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename=' . $releasedDocument['seria'] . '-' . $releasedDocument['number'] . '.pdf');
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');

                        echo file_get_contents($path);

                        //@unlink($downloadFile);
                    } else {
                        echo 'Ошибка. Файл не найден!';
                    }
                } else {
                    echo 'Не указан путь к файлу!';
                }
            } else {
                echo 'Ошибка. Отсутствуют необходимые данные для завершения операции.';
            }

            die;
        }

        //Получение файла продукта

    }

}
